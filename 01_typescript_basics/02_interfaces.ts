// const user = {
//   name: "Zura",
//   age: 28
// };


// let user: {
//   name: string,
//   age: number
//   [key: string]: any
// }

// user = {
//   name: 'Zura',
//   age: 28,
//   surname: "",
// }


// Interface

interface Person {
  name: string;
  age: number;
  [key: string]: any;
  hello(): string;
}

let user: Person = {
  name: 'ZUra',
  age: 28,
  surname: '',
  hello() {
    return `Hello from ${this.name}`
  }
}

user.age = 28;