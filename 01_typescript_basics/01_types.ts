// let age = 15;

// age = 'foot';

// console.log(age);

// explicit

// let age: number;
// age = "goot";

// console.log(age);


// function

// function add(a: number, b: number) {
//   return a + b;
// }


// const sum = add("asdfa", 2);
// console.log(sum);

// let n: null = null;
// let u: undefined = undefined;

// console.log(n)
// console.log(u)

// function someFunc(n: number) {
//   if (n % 2 === 0) {
//     return "even";
//   }
//   return null;
// }

// const value = someFunc(3)!;
// console.log(value)
// value?.substring(1)

// // void
// function hello(name: string) {
//   console.log(`Hello from ${name}`);
// }

// const msg: string = hello("Zura")


// Array
let fruits = ["orange", "apple", "banana"]
fruits[1] = true ;